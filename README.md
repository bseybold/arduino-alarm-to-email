# README #

You'll need to load the Firmata software library onto the Arduino to use it. In the email_wrapper.py code, you'll need to set a few things: the port that the Arduino is on, the input pin that the sensor is on, the threshold for sending an email, and the details of the email account. You then run the email_wrapper.py script to run the program. To help identify the threshold, the script will output the value of the sensor every polling period. You'll have to determine that value on your own though. 

# LICENSE #

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.