import pyfirmata

INPUT = pyfirmata.INPUT
OUTPUT = pyfirmata.OUTPUT

PORT = '\\.\COM3'

def exit():
    Pin._board.exit()

class Pin(object):
    _board = None
    _board_iterator = None
    _pins = []

    def __init__(self, pin_number, type):
        if Pin._board is None:
            Pin._board = pyfirmata.Arduino(PORT)
        self.board = Pin._board
        self._value = 0 #I don't know if this line is necessary
        self.pin_number = pin_number
        self.type = type
        if type == OUTPUT:
            self._pin = self.board.get_pin('d:'+str(self.pin_number)+':o')
        elif type == INPUT:
            self._pin = self.board.get_pin('d:'+str(self.pin_number)+':i')
            if Pin._board_iterator is None:
                Pin._board_iterator = pyfirmata.util.Iterator(self.board)
                Pin._board_iterator.start()
            self._pin.enable_reporting()
        else:
            raise ValueError('Unrecognized value of pin type')
        Pin._pins.append(self.pin_number)
        
    def __str__(self):
        if self.type == OUTPUT:
            return ('<Firmata.Pin: %d, output>'%self.pin_number)
        else:   
            return ('<Firmata.Pin: %d, input>'%self.pin_number)

    def __del__(self):
        if (self.type is OUTPUT):
            try:
                self._pin.write(0)
            except:
                pass
        Pin._pins.remove(self.pin_number)
        if len(Pin._pins) == 0:
            Pin._board.exit()
            Pin._board_iterator = None
            Pin._board = None

    def getvalue(self):
        if self.type is not INPUT:
            raise TypeError('Can only read from inputs')
        if self._pin.read():
            return 1
        else:
            return 0

    def setvalue(self,value):
        if self.type is not OUTPUT:
            raise TypeError('Can only write to outputs')
        if value not in {0,1}:
            raise ValueError('Can only write values 0 and 1')
        self._pin.write(value)

    def delvalue(self,value):
        if self.type is OUTPUT:
            self._pin.write(0)

    value = property(getvalue, setvalue, delvalue, "Input/Output through a\
            variable")
