#import sys
#sys.coint_flags = 0x8
import smtplib
import time
from email.mime.text import MIMEText
import wrapped_arduino as firmata

class dummy_pin():
    def __init__(self,value):
        self.value = value

# Email parameters.
server = 'smtp.mail.somewhere.com'
port   = 587
username = 'user@somewhere.com'
password = 'password'
to_address   = username
from_address = username

# Select the arduino communication port.
firmata.PORT = '\\.\COM4'
# Inputs to use.
inputs = { 
    #'sensor':firmata.Pin(2,firmata.INPUT),
    'sensor':dummy_pin(5.0),
}
# Polling period, how often to poll for io.
polling_period = 10
# What reading should trigger an email alert.
threshold = 5.0
# Whether we should exit after a message.
do_exit_after_message = True

# How many messages we should output. 0 = silent.
debug_level = 20

class Monitor():
    def __init__(self):
        try:
            while True:
                #store how long we're waiting for
                if debug_level > 2:
                    print('Waiting '+str(polling_period)+' seconds')
                time.sleep(polling_period)
                sensor_value = inputs['sensor'].value
                if debug_level > 1:
                    print('sensor value: {}'.format(sensor_value))
                if sensor_value > threshold:
                    if debug_level > 2:
                        print('sending message')
                    self.send_message('Sensor over threshold')
        finally:
            # Exit safely.
            if hasattr(inputs['sensor'],'_board'):
                firmata.exit()

    def send_message(self, text):
        print('trying to send meessage')
        # Create a text/plain message
        msg = MIMEText(text)
        msg['Subject'] = text
        msg['From'] = from_address
        msg['To'] = to_address
        s = smtplib.SMTP(server, port)
        s.ehlo()
        s.starttls()
        s.login(username, password)
        s.sendmail(from_address, [to_address], msg.as_string())
        s.close()
        if debug_level > 2:
            print('sent message')
        if do_exit_after_message:
            exit()

if __name__ == '__main__':
    Monitor()
